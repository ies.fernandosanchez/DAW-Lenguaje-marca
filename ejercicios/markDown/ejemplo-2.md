---
title: "Ejemplo Uso de RMarkdown"
author: "fer"
date: '\today'
---

<link rel='stylesheet' href='css/main.css'/>



# Título 1 (nivel de mayor jerarquía)
## Título 2 (nivel de segunda jerarquía)
### Título 3 (nivel de tercera jerarquía)

<p class="special_table"></p>

| First Header  | Second Header |
| ------------- | ------------- |
| Content Cell  | Content Cell  |
| Content Cell  | Content Cell  |



<div class="blue_table"></div>

| First Header  | Second Header |
| ------------- | ------------- |
| Content Cell  | Content Cell  |
| Content Cell  | Content Cell  |
