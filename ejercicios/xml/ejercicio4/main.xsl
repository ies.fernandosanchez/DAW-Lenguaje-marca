<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <head>
                <style>
        table{
            width: 100%;
            text-align: center;
            margin-bottom: 10px;
        }
                </style>
            </head>
            <body>

                <xsl:for-each select="concesionarios/concesionario">
                    <table border="1">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    <xsl:value-of select="./@nombre"/>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <xsl:apply-templates select = "direccion" />
                                </td>
                            </tr>

                            <tr>
                                <th>
                        Empleados
                                </th>
                                <th>
                        Stock
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <xsl:for-each select="personal/empleado">
                                            <tr>
                                                <td>
                                                    <xsl:apply-templates select="." />
                                                    <!-- <xsl:value-of select="nombreApellidos/nombre"/> -->
                                                </td>
                                            </tr>
                                        </xsl:for-each>
                                    </table>
                                </td>
                                <td>
                                    <table>
                                        <xsl:for-each select="stock/vehiculo">
                                            <tr>
                                                <td>
                                                    <xsl:apply-templates select="." />
                                                    <!-- <xsl:value-of select="nombreApellidos/nombre"/> -->
                                                </td>
                                            </tr>
                                        </xsl:for-each>
                                    </table>
                                </td>

                            </tr>

                        </tbody>
                    </table>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="direccion">
        <xsl:value-of select="calle"/>
        <xsl:value-of select="numero"/>
        <xsl:value-of select="ccaa"/>
        <xsl:value-of select="pais"/>
    </xsl:template>

    <xsl:template match="vehiculo">
        <b>
            <xsl:value-of select="./@tipo"/>
        </b>:      
        <xsl:value-of select="./@marca"/>
        <xsl:value-of select="./@numeroOcupantes"/>
        <xsl:value-of select="./@mma"/>

    </xsl:template>

    <xsl:template match="empleado">
        <b>
            <xsl:value-of select="./@dni"/>
        </b>:      
        <xsl:apply-templates select="nombreApellidos" />
    </xsl:template>

    <xsl:template match="nombreApellidos">
        <xsl:value-of select="nombre"/>
        <xsl:value-of select="apellidos"/>
    </xsl:template>

</xsl:stylesheet>