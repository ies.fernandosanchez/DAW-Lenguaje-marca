

var op1,op2,operacion  = null;

function cogeOperando(valor){
    if(op1 == null){

        document.getElementById("panelResultados").style.display = 'block';
        document.getElementById("txtOperacion").innerHTML = '';
        document.getElementById("resultado").innerHTML = '';
        op1 = valor;
        var txtOperacion = document.getElementById("txtOperacion").innerHTML;
        txtOperacion += valor;
        document.getElementById("txtOperacion").innerHTML = txtOperacion
    }else if(operacion != null){
        op2 = valor;
        var txtOperacion = document.getElementById("txtOperacion").innerHTML;
        txtOperacion += operacion+valor;
        document.getElementById("txtOperacion").innerHTML = txtOperacion

        if(operacion=='+'){
            suma()
        }else if(operacion =='-'){
            resta()
        }else{
            multiplicacion()
        }
    }else{
        alert("Debes seleccionar la operación")
    }
}

function reset()
{
    op1 = null;
    op2 = null;
    operacion = null;

}

function suma(){
    if(op1 != null && op2 != null){
        document.getElementById("resultado").innerHTML = op1 + op2;
        reset();
    }else if(op1 != null && op2 == null){
        operacion = "+";
    }
}
function resta(){
    if(op1 != null && op2 != null){
        document.getElementById("resultado").innerHTML = op1 - op2;
        reset();
    }else if(op1 != null && op2 == null){
        operacion = "-";
    }
}
function multiplicacion(){
    if(op1 != null && op2 != null){
        document.getElementById("resultado").innerHTML = op1 * op2;
        reset();
    }else if(op1 != null && op2 == null){
        operacion = "*";
    }
}