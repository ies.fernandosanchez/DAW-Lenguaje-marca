<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">        
        <catalog>
            <xsl:for-each select="catalogo/libro">
                <book>
                    <xsl:attribute name="title">
                        <xsl:value-of select="titulo"/>
                    </xsl:attribute>

                    <xsl:attribute name="datePublish">
                        <xsl:value-of select="fechaPublicacion"/>
                    </xsl:attribute>

                    <synopsis>
                        <xsl:value-of select="sinopsis"/>
                    </synopsis>                  

                    <authors>
                        <xsl:for-each select="autores/autor">
                            <author>
                                <xsl:attribute name="name">
                                    <xsl:value-of select="nombre"/>
                                </xsl:attribute>

                            </author>
                        </xsl:for-each>
                    </authors>
                </book>

            </xsl:for-each>
        </catalog>
        
    </xsl:template>
</xsl:stylesheet>