function hideItems(){
    let items =document.getElementsByClassName('itemAnimation')
    for (let index = 0; index < items.length; index++) {
        const element = items[index];
        element.classList.add('form');
    }
}

function lanzaMovimiento(item, animation){
    hideItems();
    let items =document.getElementsByClassName(item)
    items[0].classList.remove('form');
    items[0].style ='animation: '+animation+' 5s infinite;'
}