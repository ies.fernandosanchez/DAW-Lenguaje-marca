<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="rss" indent="yes"/>
    <xsl:template match="/">        
        
            <channel>
            <title>W3Schools Home Page</title>
            <link>https://www.w3schools.com</link>
            <description>Free web building tutorials</description>
            <item>
                <title>RSS Tutorial</title>
                <link>https://www.w3schools.com/xml/xml_rss.asp</link>
                <description>New RSS tutorial on W3Schools</description>
            </item>
            <item>
                <title>XML Tutorial</title>
                <link>https://www.w3schools.com/xml</link>
                <description>New XML tutorial on W3Schools</description>
            </item>

            <item>
                <title>XSL</title>
                <link>https://www.w3schools.com/xsl</link>
                <description>New XSL tutorial on W3Schools</description>
            </item>
            </channel>
        
    </xsl:template>
</xsl:stylesheet>