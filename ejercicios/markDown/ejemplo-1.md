## Título
### Subtítulo
**Este** es un ejemplo de texto que da entrada a una lista genérica de elementos:
==
- **Elemento** 1
----
- Elemento 2
- Elemento 3
Este es un ejemplo de textoS que da entrada a una lista numerada:
1. Elemento 1
2. Elemento 2
3. Elemento 3
Al texto en Markdown puedes añadirle formato como **negrita** o *cursiva* de una manera muy sencilla.

| Primera columna | Segunda columna | Tercera columna|
| - | - |  - |
| xdawd | dWD | WDWD |

[texto del enlace](https://www.google.com)

[texto del enlace](njda@bhfe.com)

njda@bhfe.com

![texto alternativo imagen](https://secure.gravatar.com/avatar/9bc1e87870bb5ce8f161d948247f61c3?s=48&d=mm&r=g 'imagen')